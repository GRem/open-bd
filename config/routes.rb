# frozen_string_literal: true

require 'sidekiq/web'

Rails.application.routes.draw do
  # Configure home (root) path
  root to: 'application#index'

  mount Sidekiq::Web => '/sidekiq' if Rails.env.development?

  # Configure Authentication
  scope 'v1' do
    use_doorkeeper scope: 'sign' do
      as tokens: ''
      skip_controllers :authorizations, :authorized_applications
    end
  end

  # Download cover
  get 'v1/books/:book_id/covers/:cover_id/images',
      to: 'image#cover'

  # Configure routes to API
  mount V1::API, at: '/'

  # User authentication
  devise_for :users,
             skip: :all,
             controllers: {
               confirmations: 'users/confirmations',
               registrations: 'users/registrations'
             }
end
