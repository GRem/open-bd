# frozen_string_literal: true

module V1
  module UserJson
    # Serialize statistique to current user
    class StatSerializer < Serializer
      attributes :book_count, :book_prices

      def book_count
        books.count
      end

      def book_prices
        books.sum(:price)
      end

      private

      def books
        object.serie.books
      end
    end
  end
end
