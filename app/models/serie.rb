# frozen_string_literal: true

# Objet represent Serie in BDD
class Serie
  include Mongoid::Document
  include Mongoid::Timestamps

  has_many :books, inverse_of: :serie
  belongs_to :user
end
