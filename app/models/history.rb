# frozen_string_literal: true

# History used for services status changed
class History
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Enum

  field :time_response, type: Integer, default: 0

  enum :state, %I[success error stop], default: :stop

  embedded_in :service
end
