# frozen_string_literal: true

module V1
  module UserJson
    # Serialize one friend
    class FriendDataSerializer < Serializer
      attributes :email, :pseudo, :items

      def friend
        User.find(object.friend_id)
      end

      def email
        friend.email
      end

      def pseudo
        friend.pseudo
      end

      def items
        friend.serie.books.count
      end
    end
  end
end
