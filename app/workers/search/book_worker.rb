# frozen_string_literal: true

module Search
  class BookWorker < BasehWorker

    def perform(*args)
      rqe = ISBNWrapper.new(args[1])

      rqe.response
    end
  end
end
