# frozen_string_literal: true

module V1
  module ServiceJson
    # Serialize resource status
    class StatusSerializer < ActiveModel::Serializer
      type :status
      attributes :id, :name, :description, :status
    end
  end
end
