# frozen_string_literal: true

module V1
  # News about Open BD services
  class NewsApi < Grape::API
    include V1::Concerns::Default

    resource :news do
      desc 'Return a list news'
      oauth2
      get do
      end

      desc 'Create a new news'
      oauth2
      post do
      end

      desc 'Show news'
      params do
        requires :id, type: String, desc: 'ID to news'
      end
      oauth2
      get ':id' do
      end
    end
  end
end
