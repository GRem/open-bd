# frozen_string_literal: true

# Configure option for Grape gem
api_folder = File.join('app', 'api')
type_files = { glob: File.join('**', '*.rb') }
autoload_path = Dir[Rails.root.join('app', 'api', '*')]

OpenBd::Application.config.paths.add api_folder, type_files
OpenBd::Application.config.autoload_paths += autoload_path

# Fix adapter : https://github.com/ruby-grape/grape-active_model_serializers/issues/81
ActiveModelSerializers.config.adapter = :json
