# frozen_string_literal: true

CarrierWave.configure do |config|
  config.storage = :grid_fs
  config.root = Rails.root.join('tmp')
  config.cache_dir = 'uploads'
  config.grid_fs_access_url = '/'

  # If you want CarrierWave to fail noisily in development,
  # you can change these configs in your environment file:
  config.ignore_integrity_errors = false
  config.ignore_processing_errors = false
  config.ignore_download_errors = false
end
