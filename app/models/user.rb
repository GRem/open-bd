# frozen_string_literal: true

# Object represent user in BDD
class User
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Enum
  include Mongoid::Search

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  # :rememberable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :trackable, :validatable, :lockable

  ## Database authenticatable
  field :email,              type: String, default: ''
  field :encrypted_password, type: String, default: ''

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String

  ## Confirmable
  field :confirmation_token,   type: String
  field :confirmed_at,         type: Time
  field :confirmation_sent_at, type: Time
  field :unconfirmed_email,    type: String

  ## Lockable
  field :failed_attempts, type: Integer, default: 0
  field :unlock_token,    type: String
  field :locked_at,       type: Time

  ## Personal information
  field :pseudo, type: String

  # Role to user
  # customer:   classic user
  # robot:      robot user for treatment specific
  # admin:      admin has possibility to see all resources
  enum :role, %I[bot customer beta developer admin root], default: :customer

  # Relations
  has_one :serie, autobuild: true
  # recursively_embeds_many # Used for saving friends
  embeds_many :friends

  # Searchs
  search_in :email, :pseudo

  # Validations
  validate :email
  validates :pseudo, presence: true, if: :customer?

  # Indexes
  index({ role: 1 }, unique: true, name: 'user_role_index')
end
