# frozen_string_literal: true

module V1
  module CollectionJson
    class BookSerializer < Serializer
      attributes :id, :title, :isbn13, :isbn10, :saga, :volume

      def saga
        object.saga.name
      end
    end
  end
end
