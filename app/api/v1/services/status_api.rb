# frozen_string_literal: true

module V1
  module Services
    # Resource status for service
    class StatusApi < Grape::API
      include V1::Concerns::Default

      resource :status do
        desc 'Status for all services'
        get each_serializer: V1::ServiceJson::StatusSerializer do
          Service.all
        end
=begin
        desc 'Update status to service'
        oauth2
        route_param :service_id do
          params do
            requires :state, type: String, desc: 'New state to service'
            optional :time, type: String, desc: 'Time response to service'
          end
          post do
            service = Service.find(params[:service_id])
            history = History.new(state: params[:state], time_response: params[:time])
            service.histories = service.histories + [history]
            service.save
          end
        end
=end
      end
    end
  end
end
