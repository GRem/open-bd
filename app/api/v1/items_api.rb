# frozen_string_literal: true

module V1
  # Manage user
  class ItemsApi < Grape::API
    include V1::Concerns::Default

    resource :items do
      params do
        requires :book_id, type: String, desc: 'Book ID.'
      end
      route_param :book_id do
        desc 'Add item to collection (book)'
        oauth2
        post do
          book = Book.find(params[:book_id])
          books = current_user.serie.book_ids
          current_user.serie.book_ids = books.push(book.id)
          current_user.serie.save
        end

        desc 'Remove an item to collection'
        oauth2
        delete do
          serie = current_user.serie
          serie.books.delete(Book.find(params[:book_id]))
          serie.save
        end
      end
    end
  end
end
