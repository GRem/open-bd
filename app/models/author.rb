# frozen_string_literal: true

# Object represent Author in BDD
class Author
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String
  field :work, type: String

  has_and_belongs_to_many :books
end
