# frozen_string_literal: true

require 'net/http'

module V1
  # Searh books
  class SearchApi < Grape::API
    resource :search do
      mount V1::Searchs::BookApi
      mount V1::Searchs::FriendApi
    end
  end
end
