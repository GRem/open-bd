# frozen_string_literal: true

module V1
  # Manage books
  class BooksApi < Grape::API
    include V1::Concerns::Default

    resource :books do
      desc 'List of books'
      oauth2
      get each_serializer: V1::BookJson::BookSerializer do
        Book.all
      end

      desc 'Create book'
      oauth2
      post do
      end

      params do
        requires :id, type: String, desc: 'Book ID'
      end
      route_param :id do
        desc 'Show book'
        oauth2
        get serializer: V1::BookJson::BookSerializer do
          Book.find(params[:id])
        end

        desc 'Update book'
        oauth2
        put do
        end

        desc 'Destroy book'
        oauth2
        delete do
        end
      end

      params do
        requires :book_id, type: String, desc: 'Book ID'
      end
      route_param :book_id do
        mount V1::CategoriesApi
        mount V1::CoversApi
        mount V1::EditorsApi
      end
    end
  end
end
