# frozen_string_literal: true

module V1
  # Manage editor for a BD
  class EditorsApi < Grape::API
    include V1::Concerns::Default

    resource :editors do
      desc 'Return a list of editors'
      oauth2
      get do
        Editor.all
      end
    end
  end
end
