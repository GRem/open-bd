# frozen_string_literal: true

module V1
  # Manage firends
  class FriendsApi < Grape::API
    include V1::Concerns::Default

    resource :friends do
      mount V1::Friends::ManagementsApi

      params do
        requires :user_id, type: String, desc: 'ID to friend remove'
      end
      route_param :user_id do
        mount V1::Friends::CollectionsApi
      end
    end
  end
end
