# frozen_string_literal: true

# Object represent Book cover in BDD
class Cover
  include Mongoid::Document
  include Mongoid::Timestamps

  mount_uploader :first, ImageUpload

  embedded_in :book
end
