# frozen_string_literal: true

def create_service(name, description, uri)
  service = Service.new(name: name, description: description, address: uri)
  history = History.new(state: 'stop')
  service.histories = [history]
  service.save
end

create_service('api', 'Backend service', 'https://api.openbd.local')
create_service('documentation', 'Documentation developer', 'https://documentation.openbd.local')
create_service('support', 'Service for support (forum, tchat ...)', 'https://support.openbd.local')
create_service('stat', 'Service for display statistique about books, user ...', 'https://stat.openbd.local')
create_service('news', 'Service for news about OpenBD', 'https://news.openbd.local')
