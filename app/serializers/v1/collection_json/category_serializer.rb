# frozen_string_literal: true

module V1
  module CollectionJson
    class CategorySerializer < Serializer
      attribute :type
    end
  end
end
