# frozen_string_literal: true

# Code concern search book in API ISBN
module V1Search
  extend ActiveSupport::Concern

  included do
    helpers do
      # Send request to ISBN API
      def request_api
        url = URI.parse "#{SEARCH_ISBN}#{params[:isbn]}"
        req = Net::HTTP::Get.new(url.to_s)
        res = Net::HTTP.start(url.host, url.port) do |http|
          http.request(req)
        end

        JSON.parse(res.body)
      end

      # Get editor or create if doesn't exist
      def editor(resultat)
        if resultat.key?('editions')
          Editor.find_or_create_by(name: resultat['editions'])
        else
          Editor.find_or_create_by(name: 'NA')
        end
      end

      # Get authors or created if doesn't exist
      def authors(resultat)
        authors = []

        if test_author_key(resultat, 'authors_mention')
          resultat['authors_mention'].each do |author|
            authors.push add_author(author.split(',')) unless author.empty?
          end
        end

        if test_author_key(resultat, 'authors_mentions')
          resultat['authors_mentions'].each do |author|
            authors.push add_author(author.split(',')) unless author.empty?
          end
        end

        if test_author_key(resultat, 'author')
          authors.push create_author(resultat, 'author')
        end

        if authors.empty?
          [Author.find_or_create_by(name: 'NA')]
        else
          authors
        end
      end

      def test_author_key(resultat, key)
        resultat.key?(key) && !resultat[key].empty?
      end

      def add_author(author_string)
        if !author_string[1].nil? && !author_string[1].empty?
          Author.find_or_create_by(name: author_string[1],
                                   work: author_string[0])
        else
          Author.find_or_create_by(name: 'NA')
        end
      end

      def create_author(resultat, key)
        author_string = resultat[key].split(',')
        Author.find_or_create_by(name: author_string[1], work: author_string[0])
      end

      # Get saga to book
      def create_saga(resultat)
        col = resultat['collection']
        saga_name = col.nil? ? resultat['title'] : col
        Saga.find_or_create_by(name: saga_name)
      end

      def volume(resultat)
        if resultat.key?('volume')
          ['tome '].each do |test|
            resultat['volume'].slice! test
          end
          resultat['volume']
        end
      end

      def price(resultat)
        if resultat.key?('price')
          resultat['price'].delete(' EUR').gsub(',', '.').to_f
        end
      end

      # Create new book
      def book(resultat)
        Book.new(isbn13: resultat['isbn'],
                 title: resultat['title'],
                 description: resultat['description'],
                 year: resultat['year'],
                 editor: editor(resultat),
                 price: price(resultat),
                 volume: volume(resultat),
                 authors: authors(resultat),
                 serie: current_user.serie)
      end
    end
  end
end
