# frozen_string_literal: true

module ApplicationCable
  class BooksChannel < ActionCable::Channel::Base
    # Called when the consumer has successfully
    # become a subscriber to this channel.
    def subscribed
      p 'Client has been subscribed !'
    end
  end
end
