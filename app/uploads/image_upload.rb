# frozen_string_literal: true

class ImageUpload < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  version :thumb do
    process resize_to_fill: [280, 280]
  end

  version :small_thumb, from_version: :thumb do
    process resize_to_fill: [20, 20]
  end

  def filename
   "#{model.id}.jpeg"
  end

  def store_dir
    "v1/books/#{model.book.id}/covers/#{model.id}/images"
  end

  def extension_whitelist
    %w(jpeg jpg png)
  end

  def content_type_whitelist
    /image\//
  end

  def content_type_blacklist
    ['application/text']
  end
end

