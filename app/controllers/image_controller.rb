# frozen_string_literal: true

class ImageController < ApplicationController
  def cover
    cover = Book.find(params[:book_id]).cover
    content = if params.key?(:filter)
                case params[:filter]
                when 'normal'
                  cover.first
                when 'thumb'
                  cover.first.thumb
                when 'small'
                  cover.first.small_thumb
                end
              else
                cover.first
              end

    send_data content.read, type: content.file.content_type, disposition: "inline"
    expires_in 0, public: true
  end
end
