# frozen_string_literal: true

# Object represent Category in BDD
class Category
  include Mongoid::Document
  include Mongoid::Timestamps

  field :type, type: String

  belongs_to :book
end
