# frozen_string_literal: true

module V1
  # Resource service
  class ServicesApi < Grape::API
    include V1::Concerns::Default

    resource :services do
      mount V1::Services::StatusApi

      desc 'List of services'
      oauth2
      paginate
      get do
        Service.all
      end

      desc 'Create a new service'
      oauth2
      post do
      end

      desc 'Get service information'
      oauth2
      params do
        requires :id, type: String, desc: 'ID to service'
      end
      get ':id' do
      end

      desc 'Update a service'
      oauth2
      params do
        requires :id, type: String, desc: 'ID to service'
      end
      put ':id' do
      end

      desc 'Delete a service'
      oauth2
      params do
        requires :id, type: String, desc: 'ID to service'
      end
      delete ':id' do
      end
    end
  end
end
