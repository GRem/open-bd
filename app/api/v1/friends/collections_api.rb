# frozen_string_literal: true

module V1
  module Friends
    class CollectionsApi < Grape::API
      include V1::Concerns::Default

      resource :collections do
        desc 'Show collection'
        oauth2
        get serializer: V1::CollectionJson::SerieSerializer do
          User.find(params[:user_id]).serie
        end
      end
    end
  end
end
