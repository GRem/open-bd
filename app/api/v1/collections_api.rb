# frozen_string_literal: true

module V1
  # Manage collection to user BD
  class CollectionsApi < Grape::API
    include V1::Concerns::Default

    resource :collections do
      desc 'Return a list of collections'
      oauth2
      paginate
      get serializer: V1::CollectionJson::SerieSerializer do
        render current_user.serie
      end

      mount V1::ItemsApi
    end
  end
end
