# frozen_string_literal: true

# Object for services
class Service
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name,        type: String
  field :description, localize: true
  field :address,     type: String

  embeds_many :histories

  def status
    if histories.empty?
      'stop'
    else
      histories.first.state
    end
  end
end
