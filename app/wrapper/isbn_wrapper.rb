# frozen_string_literal: true

class ISBNWrapper
  def initialize(params)
    @url = URI.parse "#{SEARCH_ISBN}#{params[:isbn]}"
    @req = Net::HTTP::Get.new(@url.to_s)
  end

  def response
    response = Net::HTTP.start(@url.host, @url.port) do |http|
      http.request(@req)
    end

    JSON.parse(response.body)
  end
end
