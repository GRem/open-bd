# frozen_string_literal: true

module V1
  class SignApi < Grape::API
    include V1::Concerns::Default

    resource :sign do
      desc 'Register user in service'
      params do
        requires :email, type: String, desc: 'Email to new user'
        requires :password, type: String, desc: 'Password to new user'
        requires :password2, type: String, desc: 'Password confirmation to new user'
        requires :pseudo, type: String, desc: 'Pseudonyme'
      end
      post :up do
        user = User.new(email: params[:email],
                        password: params[:password],
                        password_confirmation: params[:password2],
                        pseudo: params[:pseudo])

        if user.save
          status :no_content
        else
          error! user.errors.messages
        end
      end

      desc 'User confirm email'
      params do
        requires :confirmation_token, type: String, desc: 'Confirmation token to email'
      end
      post :confirmation do
        user = User.confirm_by_token(params[:confirmation_token])

        if user.save
          user.unset(:confirmation_token, :confirmation_sent_at)
          status :no_content
        else
          error! user.errors.messages
        end
      end
    end
  end
end
