# frozen_string_literal: true

module V1
  module UserJson
    # Serialize user
    class UserSerializer < Serializer
      attributes :id, :email, :pseudo, :items, :role

      def items
        object.serie.book_ids.count
      end
    end
  end
end
