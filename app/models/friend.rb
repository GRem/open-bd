# frozen_string_literal: true

# Object represent user in BDD
class Friend
  include Mongoid::Document
  include Mongoid::Timestamps

  field :friend_id, type: BSON::ObjectId

  embedded_in :user
  validates :friend_id, uniqueness: true
end
