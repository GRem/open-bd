# frozen_string_literal: true

class Saga
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  has_many :books
end
