# frozen_string_literal: true

module V1
  module UserJson
    class SearchFriendSerializer < Serializer
      attributes :friends

      has_many :friends, serializer: V1::UserJson::UserSerializer

      def friends
        object
      end
    end
  end
end
