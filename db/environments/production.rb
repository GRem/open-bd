# frozen_string_literal: true

Doorkeeper::Application.create(name: 'Open BD Android officiel',
                               redirect_uri: 'https://open-bd.d2go.fr',
                               uid: ENV['APP_ANDROID_UID'],
                               secret: ENV['APP_ANDROID_SECRET'])
