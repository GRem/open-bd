# frozen_string_literal: true

require 'net/http'

module V1
  module Searchs
    # Request for search book in API ISBN
    class BookApi < Grape::API
      include V1::Concerns::Default
      # include V1Search

      resource :books do
        desc 'Search a book with a isbn10 or isbn13'
        params do
          requires :isbn, type: String, desc: 'ISBN 10 or 13'
        end
        oauth2
        get ':isbn', serializer: V1::TryJson::SearchSerializer do
          # Search if book exist in database
          isbn10 = Book.find_by(isbn10: params[:isbn])
          isbn13 = Book.find_by(isbn13: params[:isbn])

          if !isbn10.nil?
            isbn10
          elsif !isbn13.nil?
            isbn13
          else
            Search::BookWorker.perform_async([params[:isbn]])

            status :processing
=begin
            resultat = request_api

            if resultat.nil?
              error! 'Not found', 404
            else
              I18n.default_locale = :fr

              rqe_debug = Request.new(search: "#{SEARCH_ISBN}#{params[:isbn]}",
                                      response: resultat)

              book = book(resultat)
              book.saga = create_saga(resultat)

              if book.save
                rqe_debug.code = 200
                rqe_debug.save
                book
              else
                rqe_debug.code = 500
                rqe_debug.save
                error! "Error search : #{book.errors.messages}", 500
              end
            end
=end
          end
        end
      end
    end
  end
end
