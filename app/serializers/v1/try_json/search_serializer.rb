# frozen_string_literal: true

module V1
  module TryJson
    class SearchSerializer < Serializer
      attributes :id, :title, :year, :isbn10, :isbn13
    end
  end
end
