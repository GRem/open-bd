# frozen_string_literal: true

class Serializer < ActiveModel::Serializer
  def attributes(attrs, option)
    hash = super
    hash.delete_if { |_key, value| value.nil? }
    hash
  end

  def to_array(serializer, datas)
    out = []
    datas.map do |data|
      sz = ActiveModelSerializers::Adapter::Json.new(serializer.new(data))
      sz.serializer.to_hash.values.each { |value| out.push(value) }
    end
    out
  end
end
