# frozen_string_literal: true

module V1
  module Friends
    class ManagementsApi < Grape::API
      include V1::Concerns::Default

      desc 'Add new friend'
      oauth2
      params do
        requires :user_id, type: String, desc: 'ID to new friend'
      end
      post ':user_id' do
        search_user = User.find(params[:user_id])
        if current_user.id != search_user.id
          if !search_user.nil? && Friend.find_by(friend_id: search_user.id).nil?
            new_friend = Friend.new(friend_id: search_user.id, user: current_user)
            p "Create friend >> #{new_friend}"

            current_user.friends.push new_friend

            if current_user.save
              p "OUAI"
              status :no_content
            else
              p "HOOOO"
              render current_user.errors.messages
            end
          else
            p "Pas d'utilisateur"
            render current_user.errors.messages
          end
        else
          status :no_content
        end
      end

      desc 'Remove friend'
      oauth2
      params do
        requires :user_id, type: String, desc: 'ID to friend remove'
      end
      delete ':user_id' do
        current_user.friends.delete User.find(params[:user_id])
        current_user.save
      end

      desc 'List your friends'
      oauth2
      paginate
      get serializer: V1::UserJson::FriendSerializer do
        current_user
      end

      desc 'Show items to your friend'
      oauth2
      paginate
      params do
        requires :user_id, type: String, desc: 'ID to friend remove'
        optional :order, type: String, default: :title, desc: 'Order used'
        optional :sort, type: String, default: :asc, desc: 'Sort used (asc or desc)'
      end
      route_param :user_id do
        get serializer: V1::CollectionJson::SerieSerializer do
          render User.find(params[:user_id]).serie, meta: {
            sort: params['sort'],
            order: params['order']
          }
        end
      end
    end
  end
end
