# frozen_string_literal: true

module CustomTokenResponse
  def body
    additional_data = {
      'pseudo' => User.find(@token.resource_owner_id).pseudo
    }

    # call original `#body` method and merge its result with the additional data hash
    super.merge(additional_data)
  end
end
