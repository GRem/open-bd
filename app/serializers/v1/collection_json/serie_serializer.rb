# frozen_string_literal: true

module V1
  module CollectionJson
    class SerieSerializer < Serializer
      attributes :books

      has_many :books, serializer: V1::CollectionJson::BookSerializer

      def books
        object.books.sort_by do |t|
          [t.saga.name, t.volume]
        end
      end
    end
  end
end
