# frozen_string_literal: true

# Code concern each class to API v1
module V1
  module Concerns
    module Default
      extend ActiveSupport::Concern

      included do
        version           'v1', using: :path
        format            :json
        formatter         :json, Grape::Formatter::ActiveModelSerializers
        default_format    :json
        use               ::WineBouncer::OAuth2

        before { set_locale }

        helpers do
          def logger
            Rails.logger
          end

          def set_locale
            I18n.locale = use_language || I18n.default_locale
          end

          def current_user
            User.find(doorkeeper_access_token.resource_owner_id) if doorkeeper_access_token
          end

          private

          def use_language
            request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first.to_sym \
              unless request.env['HTTP_ACCEPT_LANGUAGE'].nil?
          end
        end
      end
    end
  end
end
