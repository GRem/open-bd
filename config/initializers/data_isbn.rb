# frozen_string_literal: true

# Constant for search data in ISBN api's
# @see https://framagit.org/Lapin/apisbn

server_isbn =       "http://#{ENV['API_ISBN']}/v1/"
SEARCH_ISBN =       "#{server_isbn}isbn/"
SEARCH_WORD =       "#{server_isbn}guess/"
SEARCH_BNF =        "#{server_isbn}bnf/"
SEARCH_BNF_DEBUG =  "#{SEARCH_BNF}debug/"
SEACH_OPENEL =      "#{server_isbn}openel/"
SEACH_GOOGLE =      "#{server_isbn}google/"
SEACH_WCAT =        "#{server_isbn}wcat/"
