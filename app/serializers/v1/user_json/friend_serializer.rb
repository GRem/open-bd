# frozen_string_literal: true

module V1
  module UserJson
    # Serialize friends to user
    class FriendSerializer < Serializer
      attributes :friends

      has_many :friends, serializer: V1::UserJson::FriendDataSerializer
    end
  end
end
