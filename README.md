# Open BD

API for saving BD, comics, book ...

## Start with docker compose

Pull, build, Run, enjoy ...

```linux
# pull
docker-compose pull

# build
docker-compose build

# Run
docker-compose up -d

# Logs
docker-compose logs -f api

# Add default data
docker-compose exec api bundle rake db:seed
```

Access to API -> `localhost`
Access to Documentation -> `localhost:8080`

## Test with insomnia

Use insomnia for testing API.

import last file in `resources/insomnia/`
