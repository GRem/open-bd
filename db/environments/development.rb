# frozen_string_literal: true

def create_user(email, pseudo, role)
  user = User.new(email: email, password: 'yopyop', pseudo: pseudo, role: role)
  user.skip_confirmation!
  user.save
end

def create_application(name, url)
  Doorkeeper::Application.create(name: name, redirect_uri: url)
end

create_application(:Android, 'http://android.openbd.local')
create_application(:News, 'http://news.openbd.local')
create_application(:My, 'http://my.openbd.local')
create_application(:Status, 'http://status.openbd.local')
create_application(:Admin, 'http://admin.openbd.local')
create_application(:Stats, 'http://stat.openbd.local')
create_application(:Insomnia, 'http://news.openbd.local')
create_application(:Dashboard, 'http://dashboard.openbd.local')

create_user('news@openbd.local',      'news',       :bot)
create_user('status@openbd.local',    'status',     :bot)
create_user('customer@openbd.local',  'customer',   :customer)
create_user('beta@openbd.local',      'beta',       :beta)
create_user('developer@openbd.local', 'developer',  :developer)
create_user('admin@openbd.local',     'admin',      :admin)
create_user('root@openbd.local',      'root',       :root)
