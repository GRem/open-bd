# frozen_string_literal: true

# Save generic elements
seed_generic = Rails.root.join('db', 'environments', 'generic.rb')
load(seed_generic)

# Save by environments
seed_env = Rails.root.join('db', 'environments', "#{Rails.env.downcase}.rb")
load(seed_env)

# Save by local environments
seed_local = Rails.root.join('db', 'environments', "#{Rails.env.downcase}.local.rb")
load(seed_local) if File.exists?(seed_local)
