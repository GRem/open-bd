# frozen_string_literal: true

module V1
  # Manage covers to book
  class CoversApi < Grape::API
    include V1::Concerns::Default
    include ActionController::DataStreaming

    content_type :jpeg, 'image/jpeg'

    resource :covers do
      desc 'Show all covers'
      oauth2
      get do
      end

      desc 'Show cover iformation'
      params do
        requires :cover_id, type: String, desc: 'Covers ID.'
        optional :filter, type: String, desc: 'Type format to cover (normal, thumb, small)'
      end
      route_param :cover_id do
        oauth2
        get do
          cover = Book.find(params[:book_id]).cover

          if params.key?(:filter)
            case params[:filter]
            when 'normal'
              cover.first
            when 'thumb'
              cover.first.thumb
            when 'small'
              cover.first.small_thumb
            end
          else
            cover
          end
        end
      end

      desc 'Add new cover, upload a new image'
      params do
        requires :file, type: Rack::Multipart::UploadedFile, desc: 'First cover to book.'
      end
      oauth2
      post do
        new_file = ActionDispatch::Http::UploadedFile.new(params[:file])

        cover = Cover.new(first: new_file)

        book = Book.find(params[:book_id])
        book.cover = cover
        book.save
      end

      desc 'Delete cover'
      params do
        requires :cover_id, type: String, desc: 'Covers ID.'
      end
      route_param :cover_id do
        oauth2
        delete do
          cover = Book.find(params[:book_id]).cover
          cover.remove_first!
          cover.save
        end
      end
    end
  end
end
