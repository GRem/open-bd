# frozen_string_literal: true

module Users
  # Manage devise gem registration
  class RegistrationsController < Devise::RegistrationsController
    protected

    def after_sign_up_path_for(resource); end

    def after_inactive_sign_up_path_for(resource); end
  end
end
