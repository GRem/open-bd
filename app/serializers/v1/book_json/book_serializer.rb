# frozen_string_literal: true

module V1
  module BookJson
    class BookSerializer < Serializer
      attributes :id, :title, :isbn13, :isbn10, :description, :authors, :categories,
        :saga, :volume

      def authors
        to_array(V1::CollectionJson::AuthorSerializer, object.authors)
      end

      def categories
        to_array(V1::CollectionJson::CategorySerializer, object.categories)
      end

      def saga
        object.saga.name
      end
    end
  end
end
