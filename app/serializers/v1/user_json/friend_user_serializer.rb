# frozen_string_literal: true

module V1
  module UserJson
    class FriendUserSerializer < Serializer
      attributes :email, :pseudo, :items

      def email
        object.user.email
      end

      def pseudo
        object.user.pseudo
      end

      def items
        object.user.serie.books.count
      end
    end
  end
end
