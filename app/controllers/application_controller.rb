# frozen_string_literal: true

# Primary controller
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  # Return request to web site to Open BD
  # GET /
  def index
    redirect_to(ENV['APP_WEB'], status: 303)
  end
end
