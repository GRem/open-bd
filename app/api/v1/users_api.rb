# frozen_string_literal: true

module V1
  # Manage user
  class UsersApi < Grape::API
    include V1::Concerns::Default

    resource :users do
      desc 'List of users'
      oauth2
      get each_serializer: V1::UserJson::UserSerializer do
        User.all
      end

      desc 'Create a new user'
      oauth2
      post do
      end

      route_param :id do
        desc 'Get a user information'
        oauth2
        get serializer: V1::UserJson::UserSerializer do
          User.find(params[:id])
        end

        desc 'Update a user information'
        oauth2
        put do
        end

        desc 'Delete a user'
        oauth2
        delete do
        end

        mount V1::Users::BooksApi
      end
    end
  end
end
