# frozen_string_literal: true

sidekiq_conf = {
  url: ENV['REDIS_URL'],
  namespace: ENV['SIDEKIQ_NAMESPACE']
}

Sidekiq.configure_server { |config| config.redis = sidekiq_conf }
Sidekiq.configure_client { |config| config.redis = sidekiq_conf }
