# frozen_string_literal: true

module V1
  # Manage categories of books
  class CategoriesApi < Grape::API
    include V1::Concerns::Default

    resource :categories do
      desc 'List of categories'
      oauth2
      get do
        Category.all
      end

      desc 'Create a new categorie'
      oauth2
      post do
      end

      params do
        requires :id, type: String, desc: 'Category ID'
      end
      route_param :id do
        desc 'Show category'
        oauth2
        get do
        end

        desc 'Update category'
        oauth2
        put do
        end

        desc 'Destroy a category'
        oauth2
        delete do
        end
      end
    end
  end
end
