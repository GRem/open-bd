# frozen_string_literal: true

require 'i18n/backend/fallbacks'

I18n::Backend::Simple.send(:include, I18n::Backend::Fallbacks)

# Loading locales
I18n.load_path += Dir[Rails.root.join('config', 'locales', '*.yml')]

# Whitelist locales available for the application
I18n.available_locales = %i[en fr]

# Set default locale to something other than :en
I18n.default_locale = :en

# Use default locale when current locale is not available
I18n.fallbacks[:fr] = %i[fr en]
