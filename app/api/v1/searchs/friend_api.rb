# frozen_string_literal: true

module V1
  module Searchs
    # Routes for seraching user (friend) in service
    class FriendApi < Grape::API
      include V1::Concerns::Default

      resource :friends do
        desc 'Search a friend'
        oauth2
        params do
          requires :user, type: String, desc: 'Name or email to user search'
        end
        get serializer: V1::UserJson::UserSerializer do
          User.customer
              .not_in(id: current_user.id)
              .full_text_search(params[:user].downcase, match: :any)
        end
      end
    end
  end
end
