# frozen_string_literal: true

# Object for debugging. Save request sending and result to book search
class Request
  include Mongoid::Document
  include Mongoid::Timestamps

  field :search,    type: String
  field :response,  type: Hash
  field :code,      type: Integer
end
