# frozen_string_literal: true

# Configure path autoloaded by Rails framework when start
#
# If you want add new path RESTART SERVER !
[
  'app/doorkeeper',
  'app/wrapper'
].each do |path|
  Rails.configuration.autoload_paths += Dir[Rails.root.join(path)]
end
