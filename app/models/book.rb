# frozen_string_literal: true

# Object represent Book in BDD
class Book
  include Mongoid::Document
  include Mongoid::Timestamps

  field :title,         type: String
  field :isbn13,        type: Integer
  field :isbn10,        type: Integer
  field :year,          type: Integer
  field :description,   localize: true
  field :volume,        type: String
  field :price,         type: Float

  has_many :categories
  has_and_belongs_to_many :authors
  belongs_to :serie, inverse_of: :book
  belongs_to :editor
  belongs_to :saga
  embeds_one :cover

  index({ isbn13: 1 }, unique: true, name: 'index_isbn13', background: true)
  index({ isbn10: 1 }, unique: true, name: 'index_isbn10', background: true)
  index({ saga: 1 }, unique: true, name: 'index_saga', background: true)
end
