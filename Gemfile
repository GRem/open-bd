# frozen_string_literal: true

source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

gem 'api-pagination', '~> 4.7'
gem 'carrierwave-mongoid', '~> 1.0'
gem 'devise', '~> 4.4'
gem 'doorkeeper', '~> 4.2', '>= 4.2.6'
gem 'doorkeeper-i18n', '~> 4.0'
gem 'doorkeeper-mongodb', '~> 4.0'
gem 'grape', '~> 1.0'
gem 'grape-active_model_serializers', '~> 1.5'
gem 'grape-kaminari', '~> 0.1.9'
gem 'grape-swagger', '~> 0.27.3'
gem 'grape_on_rails_routes', '~> 0.3.2'
gem 'haml', '~> 5.0', '>= 5.0.4'
# gem 'i18n', '~> 0.7.0'
gem 'kaminari', '~> 1.1'
gem 'kaminari-grape', '~> 1.0'
gem 'kaminari-mongoid', '~> 1.0'
gem 'mailgun-ruby', '~> 1.1'
gem 'mini_magick', '~> 4.8'
gem 'mongoid', '~> 6.2'
gem 'mongoid-enum', git: 'https://github.com/clustertv/mongoid-enum'
gem 'mongoid-grid_fs', '~> 2.3'
gem 'mongoid_search', '~> 0.3.5'
gem 'puma', '~> 3.11'
gem 'pundit', '~> 1.1'
gem 'rack-cors', '~> 1.0'
gem 'rails', '~> 5.1'
gem 'redis-namespace', '~> 1.6'
gem 'sidekiq', '~> 5.2', '>= 5.2.3'
gem 'wine_bouncer', '~> 1.0'

group :development, :test do
  gem 'byebug', platform: :mri
end

group :local do
  gem 'brakeman', '~> 4.2', '>= 4.2.1'
  gem 'bundle-audit', '~> 0.1.0'
  gem 'haml_lint', '~> 0.27.0'
  gem 'html2haml', '~> 2.2'
  gem 'minitest', '~> 5.11', '>= 5.11.2'
  gem 'overcommit', '~> 0.44.0'
  gem 'pry-rails', '~> 0.3.6'
  gem 'rails_best_practices', '~> 1.19', '>= 1.19.1'
  gem 'rubocop', '~> 0.54.0'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
