# frozen_string_literal: true

module V1
  # See stats to current user
  class StatsApi < Grape::API
    include V1::Concerns::Default

    resource :stats do
      desc 'Statistique about users to service'
      oauth2
      get '/users' do
      end

      desc 'Statistique about books to service'
      oauth2
      get '/books' do
      end

      desc 'statistique about friends to user'
      oauth2
      params do
        requires :user_id, type: String, desc: 'user id'
      end
      get ':user_id/friends' do
      end

      desc 'statistique about books to user'
      oauth2
      params do
        requires :user_id, type: String, desc: 'user id'
      end
      get ':user_id/books' do
      end
    end
  end
end
