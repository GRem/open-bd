# frozen_string_literal: true

module V1
  # Manage authors
  class AuthorsApi < Grape::API
    include V1::Concerns::Default
    include Grape::Kaminari

    resource :authors do
      paginate per_page: 20, max_per_page: 30, offset: 5

      desc 'Return a list of authors'
      get do
        authors = Author.all
        # TODO: Fix mongoid collection and page method
        # paginate(authors)

        authors
      end

      desc 'Return a single author'
      params do
        requires :id, type: String, desc: 'Authors ID.'
      end
      route_param :id do
        get do
          Author.find(params[:id])
        end
      end

      desc 'Create a new author'
      params do
        requires :name, type: String, desc: 'Name to author'
      end
      post do
        Author.create(name: params[:name])
      end

      desc 'Update an author'
      params do
        requires :name, type: String, desc: 'Name to author'
        requires :id, type: String, desc: 'ID to author deleting'
      end
      route_param :id do
        put do
          Author.find(params[:id]).update(name: params[:name])
        end
      end

      desc 'Delete an author'
      params do
        requires :id, type: String, desc: 'ID to author deleting'
      end
      route_param :id do
        delete do
          Author.find(params[:id]).destroy
        end
      end
    end
  end
end
