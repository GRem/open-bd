# frozen_string_literal: true

module V1
  module CollectionJson
    class AuthorSerializer < Serializer
      attributes :name
    end
  end
end
