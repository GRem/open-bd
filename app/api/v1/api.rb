# frozen_string_literal: true

# Primary class for all class used in API V1
class V1::API < Grape::API
  add_swagger_documentation(
    api_version: 'v1',
    hide_documentation_path: true,
    mount_path: '/v1/documentation',
    hide_format: true
  )

  desc 'API Open BD version 1'
  version 'v1' do
    mount V1::AuthorsApi
    mount V1::BooksApi
    mount V1::CategoriesApi
    mount V1::CollectionsApi
    mount V1::EditorsApi
    mount V1::FriendsApi
    mount V1::NewsApi
    mount V1::SearchApi
    mount V1::SignApi
    mount V1::ServicesApi
    mount V1::StatsApi
    mount V1::UsersApi
  end
end
