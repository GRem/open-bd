# frozen_string_literal: true

module V1
  module Users
    # Get list books to user
    class BooksApi < Grape::API
      resource :books do
        desc 'List of books to user'
        oauth2
        get serializer: V1::CollectionJson::SerieSerializer do
          User.find(params[:id]).serie
        end
      end
    end
  end
end
