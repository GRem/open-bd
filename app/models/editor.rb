# frozen_string_literal: true

# Object represent Editor in BDD
class Editor
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  has_many :books
end
