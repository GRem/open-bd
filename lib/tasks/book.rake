# frozen_string_literal: true

namespace :book do
  desc 'Update all book with last data'
  task update: :environment do
    I18n.default_locale = :fr

    Book.all.each do |book|
      resultat = search_with_isbn(book.isbn13)

      if resultat.nil?
        next
      else
        book.update_attributes(update_book(resultat))

        saga_name = resultat['collection'].nil? ? resultat['title'] : resultat['collection']
        saga = Saga.find_or_create_by(name: saga_name)
        book.saga = saga
        book.save
      end
    end
  end

  private

  def search_with_isbn(isbn)
    url = URI.parse "#{SEARCH_ISBN}#{isbn}"
    req = Net::HTTP::Get.new(url.to_s)
    res = Net::HTTP.start(url.host, url.port) do |http|
      http.request(req)
    end

    JSON.parse(res.body)
  end

  def update_book(resultat)
    {
      isbn13: resultat['isbn'],
      title: resultat['title'],
      description: resultat['description'],
      year: resultat['year'],
      editor: editor(resultat),
      price: resultat['price'],
      volume: resultat['volume'],
      authors: authors(resultat)
    }
  end

  def editor(resultat)
    Editor.find_or_create_by(name: resultat['editions'])
  end

  def authors(resultat)
    authors = []
    resultat['authors_mention'].each do |author|
      author_string = author.split(',')
      authors.push Author.find_or_create_by(name: author_string[1], work: author_string[0])
    end

    return authors
  end
end
